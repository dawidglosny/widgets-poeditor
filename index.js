require('dotenv').config();

const http = require('http');
const connect = require('connect');
const httpProxy = require('http-proxy');
const bodyParser = require('body-parser');
const queryString = require('querystring');

const proxy = httpProxy.createProxyServer({});


function onProxyReq(proxyReq, req, res) {
  if (!req.body || !Object.keys(req.body).length) {
    return;
  }

  const contentType = proxyReq.getHeader('Content-Type');
  let bodyData;

  req.body.api_token = process.env.API_TOKEN;
  req.body.id = process.env.ID;

  if (contentType === 'application/json') {
    bodyData = JSON.stringify(req.body);
  }

  if (contentType === 'application/x-www-form-urlencoded') {
    bodyData = queryString.stringify(req.body);
  }

  if (bodyData) {
    proxyReq.setHeader('Content-Length', Buffer.byteLength(bodyData));
    proxyReq.write(bodyData);
  }
};

function onError(err, req, res) {
  res.writeHead(500, {
    'Content-Type': 'text/plain'
  });
  res.end(`Something went wrong. ${err}`);
};

proxy.on('proxyReq', onProxyReq);
proxy.on('error', onError);

const app = connect()
  .use(bodyParser.json())//json parser
  .use(bodyParser.urlencoded({ extended: true })) //urlencoded parser
  .use(function(req, res){
    proxy.web(req, res, {
      target: 'https://api.poeditor.com/v2',
      changeOrigin: true,
    });
  });

http.createServer(app).listen(5050, function() {
  console.log("listening on port 5050")
});
